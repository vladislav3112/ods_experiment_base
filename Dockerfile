FROM python:3.11

ADD . /
WORKDIR /
RUN pip install pdm==2.2.0
RUN mkdir __pypackages__ && pdm install --no-lock --no-editable
RUN pip install flask==3.0.0
ENTRYPOINT ["python3"]
CMD ["/app.py"]