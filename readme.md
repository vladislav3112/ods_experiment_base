### Проект курса ods MLOps и production в DS исследованиях
Планируется использовать методологию Data Science Lifecycle Process.
Используется [pdm](https://github.com/pdm-project/pdm) для фиксации зависимостей

#### Как запустить docker контейнер:

1. ```docker build . -t fixed_dependencies_project```
2. ```docker run -d fixed_dependencies_project```
